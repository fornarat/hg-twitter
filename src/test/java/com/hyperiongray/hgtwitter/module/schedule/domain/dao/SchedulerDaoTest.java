package com.hyperiongray.hgtwitter.module.schedule.domain.dao;

import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import com.hyperiongray.hgtwitter.module.schedule.domain.service.PatternOfLifeDateTimesGenerator;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
//@Rollback
@Commit
public class SchedulerDaoTest {

	@Autowired private SchedulerDao schedulerDao;
	@Autowired private PatternOfLifeDateTimesGenerator patternOfLifeDateTimesGenerator;


	@Test
	public void save() throws Exception {
		DateTime scheduledFor = DateTime.now();
		Schedule schedule = Schedule.newBuilder()
				.withContent("test")
				.withScreenName("test-user")
				.withScheduledFor(scheduledFor)
				.build();

		schedulerDao.save(schedule);

	}

	@Test
	public void crud() throws Exception {
	DateTime scheduledFor = patternOfLifeDateTimesGenerator.getNextDatetime(DateTime.now());

		Schedule schedule = Schedule.newBuilder()
				.withContent("test")
				.withScreenName("test-user")
				.withScheduledFor(scheduledFor)
				.build();

		schedulerDao.save(schedule);

		Schedule lastSchedule = schedulerDao.getAll().get(0);
		Assert.assertEquals(lastSchedule.getScheduledFor().getMillis(), scheduledFor.getMillis());
	}


	@Test
	public void getPending() throws Exception {

		List<Schedule> pendings = schedulerDao.getPending();
		System.out.println(pendings);
	}

}