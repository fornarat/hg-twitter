package com.hyperiongray.hgtwitter;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@org.springframework.context.annotation.Configuration
@PropertySources({
        @PropertySource(value = "file:config/properties/twitter4j.properties"),
        @PropertySource(value = "file:config/properties/database.properties"),
        @PropertySource(value = "file:config/properties-override/twitter4j.properties"),
        @PropertySource(value = "file:config/properties-override/database.properties"),
        @PropertySource(value = "file:/root/hg-twitter/config/properties-override/twitter4j.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "file:/root/hg-twitter/config/properties-override/database.properties", ignoreResourceNotFound = true),
})
//@EnableConfigurationProperties(StorageProperties.class)

//@SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})

@EnableAutoConfiguration
@ComponentScan(value = { "com.hyperiongray" })
@EnableJpaRepositories

public class Configuration {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
