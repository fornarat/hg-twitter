package com.hyperiongray.hgtwitter.module.schedule.domain.model;

import org.joda.time.DateTime;

public class Schedule {

	private Integer id;
	private String screenName;
	private String content;
	private DateTime scheduledAt;
	private DateTime scheduledFor;

	public void setId(Integer id) {
		this.id = id;
	}

	private Schedule(Builder builder) {
		id = builder.id;
		screenName = builder.screenName;
		content = builder.content;
		scheduledAt = builder.scheduledAt;
		scheduledFor = builder.scheduledFor;
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public Integer getId() {
		return id;
	}

	public String getScreenName() {
		return screenName;
	}

	public String getContent() {
		return content;
	}

	public DateTime getScheduledAt() {
		return scheduledAt;
	}

	public DateTime getScheduledFor() {
		return scheduledFor;
	}

	@Override
	public String toString() {
		return "Schedule{" +
				"id=" + id +
				", screenName='" + screenName + '\'' +
				", content='" + content + '\'' +
				", scheduledAt=" + scheduledAt +
				", scheduledFor=" + scheduledFor +
				'}';
	}

	public static final class Builder {
		private Integer id;
		private String screenName;
		private String content;
		private DateTime scheduledAt;
		private DateTime scheduledFor;

		private Builder() {}

		public Builder withId(Integer id) {
			this.id = id;
			return this;
		}

		public Builder withScreenName(String screenName) {
			this.screenName = screenName;
			return this;
		}

		public Builder withContent(String content) {
			this.content = content;
			return this;
		}

		public Builder withScheduledAt(DateTime scheduledAt) {
			this.scheduledAt = scheduledAt;
			return this;
		}

		public Builder withScheduledFor(DateTime scheduledFor) {
			this.scheduledFor = scheduledFor;
			return this;
		}

		public Schedule build() {
			return new Schedule(this);
		}
	}
}