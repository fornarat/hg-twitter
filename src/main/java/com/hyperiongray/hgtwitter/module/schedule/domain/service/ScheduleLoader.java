package com.hyperiongray.hgtwitter.module.schedule.domain.service;

import com.hyperiongray.hgtwitter.module.schedule.domain.dao.SchedulerDao;
import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class ScheduleLoader {

	@Autowired private SchedulerDao schedulerDao;
	@Autowired private TweetSchedulerService tweetSchedulerService;

	@PostConstruct
	private void loadSchedule(){
		List<Schedule> pending = schedulerDao.getPending();
		pending.stream().forEach(x -> tweetSchedulerService.schedule(x));
	}
}
