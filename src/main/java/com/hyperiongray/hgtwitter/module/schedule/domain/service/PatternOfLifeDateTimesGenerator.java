package com.hyperiongray.hgtwitter.module.schedule.domain.service;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class PatternOfLifeDateTimesGenerator {

	private Random random = new Random();

	public DateTime getNextDatetime(DateTime currentDateTime){

		int main = 14;
		double standardDeviation = 2.25;
		double randomHourOfTheDay = random.nextGaussian() * standardDeviation + main; //https://homepage.divms.uiowa.edu/~mbognar/applets/normal.html

		DateTime scheduledDateTime = parseToDatetime(currentDateTime, randomHourOfTheDay);

		if(scheduledDateTime.getMillis() < currentDateTime.getMillis()) {
			Duration duration = Duration.standardDays(1);
			scheduledDateTime = scheduledDateTime.plus(duration);
		}

		return scheduledDateTime;
	}

	private DateTime parseToDatetime(DateTime currentDateTime, double randomHourOfTheDay) {
		int hour = (int) randomHourOfTheDay;
		double minute_decimal = randomHourOfTheDay - hour;
		int minute = (int) (minute_decimal * 60);
		int second = (int) (((minute_decimal * 60)-minute) *60);
		int millis = (int) (((((minute_decimal * 60)-minute) *60) - second) * 1000);

		DateTime scheduledDateTime;
		try{
//			scheduledDateTime = new DateTime(currentDateTime.getYear(), currentDateTime.getMonthOfYear(), currentDateTime.getDayOfMonth(), hour, minute, second, DateTimeZone.UTC); // cambiar este dt para cada usuario!
			scheduledDateTime = new DateTime(currentDateTime.getYear(), currentDateTime.getMonthOfYear(), currentDateTime.getDayOfMonth(), hour, minute, second, millis);
		}
		catch (Exception e){
			scheduledDateTime = getNextDatetime(currentDateTime);
		}
		return scheduledDateTime;
	}

}
