package com.hyperiongray.hgtwitter.module.schedule.domain.service;

import com.hyperiongray.hgtwitter.module.schedule.domain.dao.SchedulerDao;
import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import com.hyperiongray.hgtwitter.module.twitter.domain.service.TwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter4j.TwitterException;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class TweetSchedulerService {

	@Autowired private TwitterService twitterService;
	@Autowired private SchedulerDao schedulerDao;


	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	class TweetTask implements Runnable{

		private TwitterService twitterService;
		private Schedule schedule;

		public TweetTask(TwitterService twitterService, Schedule schedule){
			this.twitterService = twitterService;
			this.schedule = schedule;
		}

		@Override
		public void run() {
			System.out.println("running: " + schedule);

			try {
				boolean result = twitterService.push(schedule.getScreenName(), schedule.getContent());
				schedulerDao.markProcessed(schedule, result);
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		}
	}

	public void schedule(Schedule schedule) {
		TweetTask tweetTask = new TweetTask(twitterService, schedule);
		scheduler. schedule(tweetTask, schedule.getScheduledFor().getMillis() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
	}

}
