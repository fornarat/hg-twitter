package com.hyperiongray.hgtwitter.module.schedule.domain.dao;

import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

@Repository
public class SchedulerDao {


	@Autowired private JdbcTemplate jdbcTemplate;

	public Schedule save(Schedule schedule){

		java.sql.Timestamp scheduledAt = new java.sql.Timestamp(schedule.getScheduledAt().getMillis());
		java.sql.Timestamp scheduledFor = new java.sql.Timestamp(schedule.getScheduledFor().getMillis());

//		jdbcTemplate.update("insert into SCHEDULE (SCREEN_NAME, CONTENT, SCHEDULED_AT, SCHEDULED_FOR) values (?, ?, ?, ?)",
//		                    schedule.getScreenName(), schedule.getContent(), scheduledAt, scheduledFor);
//

		SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
		jdbcInsert.withTableName("SCHEDULE").usingGeneratedKeyColumns("ID");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SCREEN_NAME", schedule.getScreenName());
		parameters.put("CONTENT", schedule.getContent());
		parameters.put("SCHEDULED_AT", scheduledAt);
		parameters.put("SCHEDULED_FOR", scheduledFor);
		// execute insert
		Number key = jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(
				parameters));
		// convert Number to Int using ((Number) key).intValue()

		schedule.setId(key.intValue());
		return schedule;

	}


	public List<Schedule> getPending(){

		Timestamp now = new Timestamp(DateTime.now().getMillis());

		List<Schedule> schedules = jdbcTemplate.query(
				"SELECT ID, SCREEN_NAME, CONTENT, SCHEDULED_AT, SCHEDULED_FOR FROM SCHEDULE WHERE SCHEDULED_FOR > ? AND STATUS IS NULL ORDER BY SCHEDULED_FOR ", new Object[] { now },
				(rs, rowNum) -> Schedule.newBuilder()
						.withId(rs.getInt("ID"))
						.withScreenName(rs.getString("SCREEN_NAME"))
						.withContent(rs.getString("CONTENT"))
						.withScheduledAt(new DateTime(rs.getTimestamp("SCHEDULED_AT").getTime()))
						.withScheduledFor(new DateTime(rs.getTimestamp("SCHEDULED_FOR").getTime()))
						.build());

		return schedules;
	}


	private DateTime getAsDatetime(java.sql.Timestamp ts){

		java.util.Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
//		java.sql.Timestamp ts = resultSet.getTimestamp(PUBLISH_TIME);
		cal.setTime(ts);
		DateTime dateTime = new DateTime(cal.getTimeInMillis());
		return dateTime;
	}

	public List<Schedule> getAll(){
		Calendar utc = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

		List<Schedule> schedules = jdbcTemplate.query(
				"SELECT ID, SCREEN_NAME, CONTENT, SCHEDULED_AT, SCHEDULED_FOR FROM SCHEDULE ORDER BY ID DESC ", new Object[] {},
				(rs, rowNum) -> Schedule.newBuilder()
						.withId(rs.getInt("ID"))
						.withScreenName(rs.getString("SCREEN_NAME"))
						.withContent(rs.getString("CONTENT"))
//						.withScheduledFor(getAsDatetime(rs.getTimestamp("SCHEDULED_FOR", utc)))
						.withScheduledAt(getAsDatetime(rs.getTimestamp("SCHEDULED_AT")))
						.withScheduledFor(getAsDatetime(rs.getTimestamp("SCHEDULED_FOR")))
						.build());


		return schedules;
	}

	public void markProcessed(Schedule schedule, boolean result){
		String resultString = "";
		if(result){
			resultString="OK";
		}
		else{
			resultString="FAIL";
		}

		jdbcTemplate.update("update SCHEDULE SET STATUS = ? WHERE ID = ?", new Object[] { resultString, schedule.getId() });
	}



}
