package com.hyperiongray.hgtwitter.module.schedule.domain.service;

import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ScheduleService {

	DateTime schedule(String screenName, String content);

	List<Schedule> getAll();
}
