package com.hyperiongray.hgtwitter.module.schedule.domain.service.impl;

import com.hyperiongray.hgtwitter.module.schedule.domain.dao.SchedulerDao;
import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import com.hyperiongray.hgtwitter.module.schedule.domain.service.PatternOfLifeDateTimesGenerator;
import com.hyperiongray.hgtwitter.module.schedule.domain.service.ScheduleService;
import com.hyperiongray.hgtwitter.module.schedule.domain.service.TweetSchedulerService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService{

	@Autowired private SchedulerDao schedulerDao;
	@Autowired private PatternOfLifeDateTimesGenerator patternOfLifeDateTimesGenerator;
	@Autowired private TweetSchedulerService tweetSchedulerService;

	@Override
	public DateTime schedule(String screenName, String content) {
		DateTime nextDatetime = patternOfLifeDateTimesGenerator.getNextDatetime(DateTime.now());
		Schedule schedule = Schedule.newBuilder().withScreenName(screenName).withContent(content)
				.withScheduledFor(nextDatetime)
				.withScheduledAt(DateTime.now())
				.build();
		Schedule savedSchedule = schedulerDao.save(schedule); // adds the id
		tweetSchedulerService.schedule(savedSchedule);
		return nextDatetime;
	}

	@Override
	public List<Schedule> getAll() {
		return schedulerDao.getAll();
	}
}
