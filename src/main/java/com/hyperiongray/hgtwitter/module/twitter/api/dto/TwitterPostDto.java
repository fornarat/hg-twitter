package com.hyperiongray.hgtwitter.module.twitter.api.dto;

public class TwitterPostDto {
	private String uuid;
	private String message;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "TwitterPostDto{" +
				"uuid='" + uuid + '\'' +
				", message='" + message + '\'' +
				'}';
	}
}
