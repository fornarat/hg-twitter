package com.hyperiongray.hgtwitter.module.twitter.controller;

import com.hyperiongray.hgtwitter.module.twitter.api.dto.ScheduleDto;
import com.hyperiongray.hgtwitter.module.twitter.api.dto.TwitterPostDto;
import com.hyperiongray.hgtwitter.module.twitter.api.service.TwitterServiceApi;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import twitter4j.TwitterException;

import java.util.List;

@RestController
public class TwitterController {

	@Autowired private TwitterServiceApi twitterServiceApi;

	@PostMapping(path = ApiRoute.TWITTER, produces = "application/json")
	public String push(@RequestBody TwitterPostDto twitterPostDto) {

		try {
			boolean result = twitterServiceApi.push(twitterPostDto.getUuid(), twitterPostDto.getMessage());
			return "{'result': "+ result +" }";
		} catch (TwitterException e) {
			e.printStackTrace();
			return "{'result': Operation Failed }";
		}
	}


	@PostMapping(path = ApiRoute.TWITTER_SCHEDULE, produces = "application/json")
	@ResponseBody
	public ScheduleDto schedule(@RequestBody TwitterPostDto twitterPostDto) {
		ScheduleDto scheduleDto = twitterServiceApi.schedule(twitterPostDto.getUuid(), twitterPostDto.getMessage());
		return scheduleDto;
	}


	@GetMapping(path = ApiRoute.TWITTER_SCHEDULE, produces = "application/json")
	@ResponseBody
	public List<ScheduleDto> schedule() {
		return twitterServiceApi.getAll();
	}

}
