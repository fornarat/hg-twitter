package com.hyperiongray.hgtwitter.module.twitter.controller;

import com.hyperiongray.hgtwitter.module.twitter.domain.dao.AccountDao;
import com.hyperiongray.hgtwitter.module.twitter.domain.dao.RequestTokenDao;
import com.hyperiongray.hgtwitter.module.twitter.domain.model.Account;
import com.hyperiongray.hgtwitter.module.twitter.domain.model.RequestTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.UUID;

@Controller
public class LoginController {


	@Value("${oauth.consumerKey}")
	private String consumerKey;

	@Value("${oauth.consumerSecret}")
	private String consumerSecret;

	private String callbackHost = "http://localhost:8080";

	@Autowired private RequestTokenDao requestTokenDao;

	@Autowired private AccountDao accountDao;

//	private Twitter twitter = new TwitterFactory().getInstance();
//	private RequestToken requestToken;

	@GetMapping(ApiRoute.TWITTER_LOGIN)
	public String login() {

//		twitter.setOAuthConsumer(consumerKey, consumerSecret);


//		request.getSession().setAttribute("twitter", twitter);
		try {
//			StringBuffer callbackURLBuffer = request.getRequestURL();
//			int index = callbackURLBuffer.lastIndexOf("/");
//			callbackURLBuffer.replace(index, callbackURLBuffer.length(), "").append("/callback"+"/" + screenName);
//			String callbackUrl = callbackURLBuffer.toString();

			String randomUUID = UUID.randomUUID().toString();
			String callbackUrl = callbackHost + "/twitter/callback/" + randomUUID;
//			Twitter twitter = TwitterFactory.getSingleton();
			Twitter twitter = new TwitterFactory().getInstance();
			twitter.setOAuthConsumer(consumerKey, consumerSecret);

			//			AccessToken oAuthAccessToken = twitter.getOAuthAccessToken();
			RequestToken requestToken = twitter.getOAuthRequestToken(callbackUrl);
			//			request.getSession().setAttribute("requestToken", requestToken);
//			response.sendRedirect(requestToken.getAuthenticationURL());

			RequestTokenDto requestTokenDto = RequestTokenDto.newBuilder().withToken(requestToken.getToken()).withTokenSecret(requestToken.getTokenSecret()).build();
			requestTokenDao.save(randomUUID, requestTokenDto);

			String authenticationURL = requestToken.getAuthenticationURL();
//			return new RedirectView(authenticationURL);
			return "redirect:" + authenticationURL;
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return null;
	}


	@GetMapping(ApiRoute.TWITTER_CALLBACK + "/{uuid}")
	public String callback(@PathVariable String uuid, HttpServletRequest request, Model model) {
//		Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
//		RequestToken requestToken = (RequestToken) request.getSession().getAttribute("requestToken");
		String verifier = request.getParameter("oauth_verifier");
		try {

			Optional<RequestTokenDto> requestTokenDtoOptional = requestTokenDao.get(uuid);
			if(!requestTokenDtoOptional.isPresent()){
				System.out.println("NO REQUEST TOKEN FOR" + uuid);
			}
			RequestTokenDto requestTokenDto = requestTokenDtoOptional.get();
			RequestToken requestToken = new RequestToken(requestTokenDto.getToken(), requestTokenDto.getTokenSecret());

//			Twitter twitter = TwitterFactory.getSingleton();
			Twitter twitter = new TwitterFactory().getInstance();
			twitter.setOAuthConsumer(consumerKey, consumerSecret);
			AccessToken oAuthAccessToken = twitter.getOAuthAccessToken(requestToken, verifier);
			//			request.getSession().removeAttribute("requestToken");
//			response.sendRedirect(request.getContextPath() + "/");
//			Status status = twitter.updateStatus("Successfully updated the status");
//			System.out.println("Successfully updated the status to [" + status.getText() + "].");

			String screenName = oAuthAccessToken.getScreenName();
			Account account = Account.newBuilder()
					.withScreenName(screenName)
					.withAccessToken(oAuthAccessToken.getToken())
					.withAccessTokenSecret(oAuthAccessToken.getTokenSecret())
					.withUserId(oAuthAccessToken.getUserId())
					.withUuid(uuid)
					.build();

			accountDao.save(account);
			model.addAttribute("screenName", screenName);
			model.addAttribute("uuid", uuid);

		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return "registered";

	}
}
