package com.hyperiongray.hgtwitter.module.twitter.domain.model;

public class RequestTokenDto {
	private String token;
	private String tokenSecret;

	private RequestTokenDto(Builder builder) {
		setToken(builder.token);
		setTokenSecret(builder.tokenSecret);
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTokenSecret() {
		return tokenSecret;
	}

	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}

	@Override
	public String toString() {
		return "RequestTokenDto{" +
				", token='" + token + '\'' +
				", tokenSecret='" + tokenSecret + '\'' +
				'}';
	}

	public static final class Builder {
		private String token;
		private String tokenSecret;

		private Builder() {}

		public Builder withToken(String token) {
			this.token = token;
			return this;
		}

		public Builder withTokenSecret(String tokenSecret) {
			this.tokenSecret = tokenSecret;
			return this;
		}

		public RequestTokenDto build() {
			return new RequestTokenDto(this);
		}
	}
}
