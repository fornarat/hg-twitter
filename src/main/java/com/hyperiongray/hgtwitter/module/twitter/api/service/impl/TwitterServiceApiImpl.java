package com.hyperiongray.hgtwitter.module.twitter.api.service.impl;

import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import com.hyperiongray.hgtwitter.module.schedule.domain.service.ScheduleService;
import com.hyperiongray.hgtwitter.module.twitter.api.dto.ScheduleDto;
import com.hyperiongray.hgtwitter.module.twitter.api.service.TwitterServiceApi;
import com.hyperiongray.hgtwitter.module.twitter.api.service.translator.ScheduleTranslator;
import com.hyperiongray.hgtwitter.module.twitter.domain.service.TwitterService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter4j.TwitterException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TwitterServiceApiImpl implements TwitterServiceApi {

	@Autowired private TwitterService twitterService;
	@Autowired private ScheduleService scheduleService;
	@Autowired private ScheduleTranslator scheduleTranslator;

	@Override
	public boolean push(String screenName, String message) throws TwitterException {
		return twitterService.push(screenName, message);
	}

	@Override
	public ScheduleDto schedule(String screenName, String content) {
		DateTime scheduledFor = scheduleService.schedule(screenName, content);
		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setScreenName(screenName);
		scheduleDto.setContent(content);
		scheduleDto.setScheduledFor(scheduledFor.toString());
		return scheduleDto;
	}

	@Override
	public List<ScheduleDto> getAll() {
		return scheduleService.getAll().stream().map(x-> scheduleTranslator.translate(x)).collect(Collectors.toList());
	}

}