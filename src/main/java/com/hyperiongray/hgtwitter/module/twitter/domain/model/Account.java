package com.hyperiongray.hgtwitter.module.twitter.domain.model;

public class Account {

	private String screenName;
	private String accessToken;
	private String accessTokenSecret;
	private Long userId;
	private String uuid;

	private Account(Builder builder) {
		screenName = builder.screenName;
		accessToken = builder.accessToken;
		accessTokenSecret = builder.accessTokenSecret;
		userId = builder.userId;
		uuid = builder.uuid;
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public String getScreenName() {
		return screenName;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public String getAccessTokenSecret() {
		return accessTokenSecret;
	}

	public Long getUserId() {
		return userId;
	}

	public String getUuid() {
		return uuid;
	}

	@Override
	public String toString() {
		return "Account{" +
				"screenName='" + screenName + '\'' +
				", accessToken='" + accessToken + '\'' +
				", accessTokenSecret='" + accessTokenSecret + '\'' +
				", userId=" + userId +
				", uuid='" + uuid + '\'' +
				'}';
	}

	public static final class Builder {
		private String screenName;
		private String accessToken;
		private String accessTokenSecret;
		private Long userId;
		private String uuid;

		private Builder() {}

		public Builder withScreenName(String screenName) {
			this.screenName = screenName;
			return this;
		}

		public Builder withAccessToken(String accessToken) {
			this.accessToken = accessToken;
			return this;
		}

		public Builder withAccessTokenSecret(String accessTokenSecret) {
			this.accessTokenSecret = accessTokenSecret;
			return this;
		}

		public Builder withUserId(Long userId) {
			this.userId = userId;
			return this;
		}

		public Builder withUuid(String uuid) {
			this.uuid = uuid;
			return this;
		}

		public Account build() {
			return new Account(this);
		}
	}
}