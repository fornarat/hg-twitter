package com.hyperiongray.hgtwitter.module.twitter.domain.dao;

import com.hyperiongray.hgtwitter.module.twitter.domain.model.RequestTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class RequestTokenDao {

	@Autowired private JdbcTemplate jdbcTemplate;

	public void save(String uuid , RequestTokenDto requestTokenDto){
		jdbcTemplate.update("insert into REQUEST_TOKEN (UUID, TOKEN, TOKEN_SECRET) values (?, ?, ?)",
		                    uuid, requestTokenDto.getToken(), requestTokenDto.getTokenSecret());
	}

	public Optional<RequestTokenDto> get(String uuid){
		List<RequestTokenDto> requestTokenDtos = jdbcTemplate.query(
				"SELECT TOKEN, TOKEN_SECRET FROM REQUEST_TOKEN WHERE UUID = ? ORDER BY TS DESC LIMIT 1 ", new Object[] { uuid },
				(rs, rowNum) -> RequestTokenDto.newBuilder()
						.withToken(rs.getString("TOKEN"))
						.withTokenSecret(rs.getString("TOKEN_SECRET"))
						.build());

		if(requestTokenDtos.isEmpty()){
			return Optional.empty();
		}
		else{
			return Optional.of(requestTokenDtos.get(0));
		}
	}
}
