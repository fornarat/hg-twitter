package com.hyperiongray.hgtwitter.module.twitter.api.dto;

import org.joda.time.DateTime;

public class ScheduleDto {

	private Integer id;
	private String screenName;
	private String content;
	private String scheduledAt;
	private String scheduledFor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getScheduledAt() {
		return scheduledAt;
	}

	public void setScheduledAt(String scheduledAt) {
		this.scheduledAt = scheduledAt;
	}

	public String getScheduledFor() {
		return scheduledFor;
	}

	public void setScheduledFor(String scheduledFor) {
		this.scheduledFor = scheduledFor;
	}

	@Override
	public String toString() {
		return "ScheduleDto{" +
				"id=" + id +
				", screenName='" + screenName + '\'' +
				", content='" + content + '\'' +
				", scheduledAt='" + scheduledAt + '\'' +
				", scheduledFor='" + scheduledFor + '\'' +
				'}';
	}
}
