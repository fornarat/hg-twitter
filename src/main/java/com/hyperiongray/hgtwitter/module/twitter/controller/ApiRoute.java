package com.hyperiongray.hgtwitter.module.twitter.controller;

public interface ApiRoute {

	String TWITTER_LOGIN = "/twitter/login";
	String TWITTER_CALLBACK = "/twitter/callback";
	String TWITTER = "/api/twitter";
	String TWITTER_SCHEDULE = "/api/twitter/schedule";
}
