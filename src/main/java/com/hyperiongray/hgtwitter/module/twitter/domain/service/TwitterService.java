package com.hyperiongray.hgtwitter.module.twitter.domain.service;

import twitter4j.TwitterException;

public interface TwitterService {

	boolean push(String screenName, String message) throws TwitterException;

}
