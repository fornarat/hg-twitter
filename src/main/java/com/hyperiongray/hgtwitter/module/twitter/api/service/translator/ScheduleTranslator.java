package com.hyperiongray.hgtwitter.module.twitter.api.service.translator;

import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import com.hyperiongray.hgtwitter.module.twitter.api.dto.ScheduleDto;
import org.springframework.stereotype.Component;

@Component
public class ScheduleTranslator {

	public ScheduleDto translate(Schedule schedule){
		ScheduleDto scheduleDto = new ScheduleDto();
		scheduleDto.setId(schedule.getId());
		scheduleDto.setScreenName(schedule.getScreenName());
		scheduleDto.setContent(schedule.getContent());
		scheduleDto.setScheduledAt(schedule.getScheduledAt().toString());
		scheduleDto.setScheduledFor(schedule.getScheduledFor().toString());
		return scheduleDto;
	}


}
