package com.hyperiongray.hgtwitter.module.twitter.domain.service.impl;

import com.hyperiongray.hgtwitter.module.schedule.domain.model.Schedule;
import com.hyperiongray.hgtwitter.module.twitter.domain.dao.AccountDao;
import com.hyperiongray.hgtwitter.module.twitter.domain.model.Account;
import com.hyperiongray.hgtwitter.module.twitter.domain.service.TwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import twitter4j.*;
import twitter4j.auth.AccessToken;

import java.util.List;
import java.util.Optional;

@Service
public class TwitterServiceImpl implements TwitterService {

	@Value("${oauth.consumerKey}")
	private String consumerKey;

	@Value("${oauth.consumerSecret}")
	private String consumerSecret;

	@Autowired private AccountDao accountDao;

	@Override
	public boolean push(String screenName, String message) throws TwitterException {
		Optional<Account> accountOptional = accountDao.getByScreenName(screenName);
		if(accountOptional.isPresent()){
//			twitter.setOAuthConsumer(consumerKey, consumerSecret);

			Account account = accountOptional.get();
			String token = account.getAccessToken();
			String tokenSecret = account.getAccessTokenSecret();
			AccessToken accessToken = new AccessToken(token, tokenSecret);
			Twitter twitter = new TwitterFactory().getInstance();
			twitter.setOAuthConsumer(consumerKey, consumerSecret);
			twitter.setOAuthAccessToken(accessToken);
			Status status = twitter.updateStatus(message);
			System.out.println("Successfully updated the status to [" + status.getText() + "].");
			return true;
		}
		else{
			return false;
		}
	}

}
