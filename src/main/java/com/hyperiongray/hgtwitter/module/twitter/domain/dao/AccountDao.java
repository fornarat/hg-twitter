package com.hyperiongray.hgtwitter.module.twitter.domain.dao;

import com.hyperiongray.hgtwitter.module.twitter.domain.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AccountDao {

	@Autowired private JdbcTemplate jdbcTemplate;

	public void save(Account account){
		jdbcTemplate.update("insert into ACCOUNT (SCREEN_NAME, OAUTH_TOKEN, OAUTH_TOKEN_SECRET, USER_ID, UUID) values (?, ?, ?, ?, ?)",
		                    account.getScreenName(), account.getAccessToken(), account.getAccessTokenSecret(), account.getUserId(), account.getUuid());
	}


	public Optional<Account> getByUuid(String uuid){
		List<Account> accounts = jdbcTemplate.query(
				"SELECT SCREEN_NAME, OAUTH_TOKEN, OAUTH_TOKEN_SECRET, USER_ID, UUID FROM ACCOUNT WHERE UUID = ? ORDER BY TS DESC LIMIT 1 ", new Object[] { uuid },
				(rs, rowNum) -> Account.newBuilder()
						.withScreenName(rs.getString("SCREEN_NAME"))
						.withAccessToken(rs.getString("OAUTH_TOKEN"))
						.withAccessTokenSecret(rs.getString("OAUTH_TOKEN_SECRET"))
						.withUserId(rs.getLong("USER_ID"))
						.withUuid(rs.getString("UUID"))
						.build());

		if(accounts.isEmpty()){
			return Optional.empty();
		}
		else{
			return Optional.of(accounts.get(0));
		}
	}

	public Optional<Account> getByScreenName(String screenName){
		List<Account> accounts = jdbcTemplate.query(
				"SELECT SCREEN_NAME, OAUTH_TOKEN, OAUTH_TOKEN_SECRET, USER_ID, UUID FROM ACCOUNT WHERE SCREEN_NAME = ? ORDER BY TS DESC LIMIT 1 ", new Object[] { screenName },
				(rs, rowNum) -> Account.newBuilder()
						.withScreenName(rs.getString("SCREEN_NAME"))
						.withAccessToken(rs.getString("OAUTH_TOKEN"))
						.withAccessTokenSecret(rs.getString("OAUTH_TOKEN_SECRET"))
						.withUserId(rs.getLong("USER_ID"))
						.withUuid(rs.getString("UUID"))
						.build());

		if(accounts.isEmpty()){
			return Optional.empty();
		}
		else{
			return Optional.of(accounts.get(0));
		}
	}

}
