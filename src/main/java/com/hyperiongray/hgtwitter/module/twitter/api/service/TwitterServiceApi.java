package com.hyperiongray.hgtwitter.module.twitter.api.service;

import com.hyperiongray.hgtwitter.module.twitter.api.dto.ScheduleDto;
import org.joda.time.DateTime;
import twitter4j.TwitterException;

import java.util.List;

public interface TwitterServiceApi {

	boolean push(String uuid, String message) throws TwitterException;

	ScheduleDto schedule(String screenName, String content);
	List<ScheduleDto> getAll();

}
