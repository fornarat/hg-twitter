package com.hyperiongray.hgtwitter.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@Order(2)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

//	@Autowired CustomLogoutSuccessHandler customLogoutSuccessHandler;
//	@Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;

//	@Autowired private UserService userService;

//	@Autowired private TokenResponseDtoFactory tokenResponseDtoFactory;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests()
				.antMatchers("/", "/home", "/home/**", "/index", "/index.html").permitAll()
				.antMatchers("/fragments/**").permitAll()
				.antMatchers("/backoffice").permitAll()
				.antMatchers("/backoffice/login").permitAll()
				.antMatchers("/backoffice/logout").permitAll()
				.antMatchers("/backoffice/signup").permitAll()
				.antMatchers("/backoffice/forgot-password").permitAll()
				.antMatchers("/backoffice/update-password-with-recovery-code").permitAll()
				.antMatchers("/backoffice/**").hasAuthority("USER")
				.and()
//				.addFilter(new JwtAuthenticationFilter(bCryptPasswordEncoder, userService, tokenResponseDtoFactory))
//				.addFilter(new JwtHeaderAuthorizationFilter(authenticationManager()))
//				.addFilter(new JwtCookieAuthorizationFilter(authenticationManager()))
				.exceptionHandling()
				.accessDeniedPage("/access-denied")
				// this disables session creation on Spring Security
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.formLogin()
				//.usernameParameter("email")
				//.passwordParameter("password")
				.loginPage("/backoffice/login")
				.defaultSuccessUrl("/backoffice/dashboard")
				.failureUrl("/backoffice/login-error.html");


		http.logout().logoutSuccessUrl("/backoffice");

	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web
				.ignoring()
				.antMatchers("/resources/**",
				             "/static/**", "/css/**", "/js/**", "/images/**",
				             "/assets/**", "favicon.ico");
	}

}
