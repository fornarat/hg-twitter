package com.hyperiongray.hgtwitter.config.security;

//import com.solvix.modules.security.controller.ApiRoutes;
//import com.solvix.modules.security.filter.JwtHeaderAuthorizationFilter;
//import com.solvix.modules.security.service.UserService;
import com.hyperiongray.hgtwitter.module.twitter.controller.ApiRoute;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

//import static com.solvix.modules.security.controller.ApiRoutes.LOGOUT_URL;
//import static com.solvix.modules.security.controller.ApiRoutes.SIGN_UP_URL;
//import static com.solvix.modules.user.controller.ApiRoutes.FORGOT_PASSWORD_ENDPOINT;
//import static com.solvix.modules.user.controller.ApiRoutes.REGISTRATION_ENDPOINT;


@Configuration
@EnableWebSecurity
@Order(1)
public class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {


//	@Autowired private UserService userService;
//	@Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.cors().and().csrf().disable()
				.authorizeRequests()

				.antMatchers(HttpMethod.GET, ApiRoute.TWITTER_LOGIN).permitAll()
				.antMatchers(HttpMethod.GET, ApiRoute.TWITTER_CALLBACK).permitAll()
				.antMatchers(HttpMethod.POST, ApiRoute.TWITTER).permitAll()
				.antMatchers(ApiRoute.TWITTER_SCHEDULE).permitAll()
				.and()
				.antMatcher("/api/**")
				.authorizeRequests()
				.anyRequest().hasAnyRole("ADMIN", "USER")
				.anyRequest().authenticated()
				.and()
//				.addFilter(new JwtHeaderAuthorizationFilter(authenticationManager()))
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}

}
