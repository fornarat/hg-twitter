package com.hyperiongray.hgtwitter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;

import java.util.TimeZone;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {


    @Value("${oauth.consumerKey}")
    private String consumerKey;

    @Value("${oauth.consumerSecret}")
    private String consumerSecret;


    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }


//    @Autowired private PushTweetService pushTweetService;
//    @Autowired private LoginService loginService;
//
//    public static void main(String[] args) throws Exception {
//        SpringApplication.run(Application.class, args);
//    }


    private void start() throws Exception {

//        Tweet tweet = new Tweet();
//        tweet.setStatus("Today was a big day!");
//        pushTweetService.push(tweet);
//        loginService.login();
//        simpleLoginService.start();
//        Twitter twitter = TwitterFactory.getSingleton();
//
//        //			Twitter twitter = new TwitterFactory.getSingleton();
//        twitter.setOAuthConsumer(consumerKey, consumerSecret);

    }


    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

//            System.out.println("Let's inspect the beans provided by Spring Boot:");
//
//            String[] beanNames = ctx.getBeanDefinitionNames();
//            Arrays.sort(beanNames);//            System.out.println("Let's inspect the beans provided by Spring Boot:");
//
//            String[] beanNames = ctx.getBeanDefinitionNames();
//            Arrays.sort(beanNames);
//            for (String beanName : beanNames) {
//                System.out.println(beanName);
//            }
//
//

//            for (String beanName : beanNames) {
//                System.out.println(beanName);
//            }
//
//
//            TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//            System.setProperty("user.timezone", "UTC");
            start();
        };
    }
}